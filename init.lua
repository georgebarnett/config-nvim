require('config.editing')
require('config.visual')
require('config.keybindings')
require('plugins')

-- Compatability with base16-shell
-- See: https://github.com/chriskempson/base16-shell#base16-vim-users
vim.cmd [[
  let base16colorspace=256
  source ~/.vimrc_background
]]

-- Highlight on yank.
vim.cmd [[
  augroup highlight_yank
    autocmd!
    au TextYankPost * silent! lua vim.highlight.on_yank{higroup="IncSearch", timeout=700}
  augroup END
]]

-- Remove trailing whitespace on write
vim.cmd [[
  autocmd BufWritePre * :%s/\s\+$//e
]]
