Neovim Config
=============

Install using:
```
wget -qO- https://gitlab.com/georgebarnett/config-nvim/raw/master/setup.sh | bash
```
