return require("packer").startup(function()
  use {'lewis6991/impatient.nvim', config = [[require('impatient')]]}
  use "wbthomason/packer.nvim"

  -- Start screen
  use {
    "goolord/alpha-nvim",
    config = [[require('config.plugin.alpha-nvim')]]
  }

  -- Autocompletion + "kind" pictographs
  use {
    "onsails/lspkind-nvim",
    event = "VimEnter"
  }

  use {
    "hrsh7th/nvim-cmp",
    after = "lspkind-nvim",
    config = [[require("config.plugin.nvim-cmp")]]
  }

  -- Autocompletion sources
  use {
    "hrsh7th/cmp-nvim-lsp",
    after = "nvim-cmp"
  }

  use {
    "hrsh7th/cmp-nvim-lua",
    after = "nvim-cmp"
  }

  use {
    "hrsh7th/cmp-path",
    after = "nvim-cmp"
  }

  use {
    "hrsh7th/cmp-buffer",
    after = "nvim-cmp"
  }

  use {
    "neovim/nvim-lspconfig",
    after = "cmp-nvim-lsp",
    config = [[require("config.plugin.lsp")]]
  }

  use {
    "nvim-telescope/telescope.nvim",
    requires = { {"nvim-lua/plenary.nvim"} }
  }


  -- Sensible defaults.
  use "tpope/vim-sensible"

  -- Comment toggling:
  -- E.g.:
  -- - gcc to toggle whether a line is commented
  -- - gcap to toggle whether a paragraph is commented
  -- - :g/re/Commentary to toggle whether a pattern is commented
  use "tpope/vim-commentary"

  -- Change surrounds around objects.
  -- E.g.:
  -- - change surrounding from """ to """ with cs""
  -- - delete surrounding """ with ds"
  -- - insert surrounding parentheses with ysiw)
  use "tpope/vim-surround"

  -- Case coersion:
  -- E.g.:
  -- - crs to convert to snake_case
  -- - crc to convert to camelCase
  -- - cru to convert to UPPERT_CASE
  -- - cr- to convert to dash-case
  -- - cr<space> to convert to space case
  --   crt to convert to Title Case
  use "tpope/vim-abolish"

  -- Colors
  use "chriskempson/base16-vim"
end)
