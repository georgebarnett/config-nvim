-- Set the leader key to space
vim.g.mapleader = ' '

-- Write with <Leader>w
vim.api.nvim_set_keymap('n', '<Leader>w', ':write<CR>', {noremap = true})

-- Copy and paste to system clipboard
vim.api.nvim_set_keymap('v', '<Leader>y', '"+y', {})
vim.api.nvim_set_keymap('v', '<Leader>p', '"+p', {})
vim.api.nvim_set_keymap('n', '<Leader>p', '"+p', {})

-- Telescope
vim.api.nvim_set_keymap('n', '<Leader>f', '<cmd>lua require(\'telescope.builtin\').find_files()<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', '<Leader>g', '<cmd>lua require(\'telescope.builtin\').live_grep()<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', '<Leader>b', '<cmd>lua require(\'telescope.builtin\').buffers()<cr>', {noremap = true})
