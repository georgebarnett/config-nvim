vim.opt.splitbelow = true
vim.opt.splitright = true

vim.opt.number = true
vim.opt.numberwidth = 4

vim.opt.list = true
vim.opt.listchars = { trail = '·', nbsp = '~' }
