-- Text width
vim.opt.linebreak = true
vim.opt.textwidth = 120
vim.opt.autoindent = true

-- Default tab size of 2, indent operators use 2 spaces
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.smartindent = true

-- Tab inserts spaces infront of a line
vim.opt.smarttab = true

-- Don't insert two spaces after '.', '?' and '!' with a join command.
vim.opt.joinspaces = false

-- Persistent undo
vim.opt.undofile = true
